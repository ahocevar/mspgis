/*
MSPGIS Geodata Viewer
Copyright (c) 2010-2012 Andreas Hocevar.  This program is free software: you
can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version. See
<http://www.gnu.org/licenses/>.

Includes GeoExt
Copyright (c) 2008-2012 The Open Source Geospatial Foundation.  Published under
the BSD license.  See http://svn.geoext.org/core/trunk/geoext/license.txt for
the full text of the license.

Inclues OpenLayers
Copyright 2005-2008 MetaCarta, Inc., released under the Clear BSD license.
Please see http://svn.openlayers.org/trunk/openlayers/license.txt for the full
text of the license.

Includes Base64 encode / decode
http://www.webtoolkit.info/
*/