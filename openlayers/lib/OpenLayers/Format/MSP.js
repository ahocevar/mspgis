OpenLayers.Format.MSP = new OpenLayers.Class(OpenLayers.Format.XML, {
    
    /**
     * Constant: NO_PATHID_KEY
     */
    NO_PATHID_KEY: "path",

    /**
     * APIProperty: measuredPath
     * {Array} of point features (<OpenLayers.Feature.Vector> with point
     * geometries). This property must be set in the initialization options.
     */
    measurePoints: null,
    
    /**
     * APIProperty: measureField
     * {String} name of the feature attribute of the <measurePoints> that
     * holds the measure for the feature. This property must be set in the
     * initialization options.
     */
    measureField: null,
    
    /**
     * APIProperty: measurePathIdField
     * {String} name of an additional feature attribute of the <measurePoints>
     * that defines the path the point belongs to. This only has to be set if
     * the <measurePoints> contain points from multiple paths. If it is set,
     * the <getMeasurePathId> method has to be defined properly for this
     * instance to find the matching path in the features that are about to be
     * created. If required, this property has to be passed with the
     * initialization options.
     */
    measurePathIdField: null,
    
    /**
     * APIProperty: attributeParsers
     * {Object} hash of functions with "value" and "attributes" arguments,
     *     keyed by the name of the field to parse. Will be called with the
     *     value to parse as first, and the attributes hash to write the
     *     value to as second argument, e.g.
     *     
     * (code)
     * attributeParsers: {
     *     "ows_Thumb": function(value, attributes) {
     *         attributes["thumb"] = value.split(",")[0];
     *     },
     *     "ows_FileRef": function(value, attributes) {
     *         attributes["pdf"] = "/" + value.split("#")[1];
     *     }
     * }
     * (end)
     */
    attributeParsers: {
        "ows_Title": function(value, attributes) {
            attributes["kmText"] = value;
        },
        "ows_Thumb": function(value, attributes) {
            attributes["thumb"] = value.split(",")[0];
        },
        "ows_FileRef": function(value, attributes) {
            attributes["pdf"] = "/" + value.split("#")[1];
        }
    },
    
    /**
     * Property: measureHash
     * {Object} a hash that will be created upon initialization. Keyed by the
     * values found in the <measurePathIdField>, it holds sorted arrays of the
     * measure points for each path.
     */
    measureHash: null,

    
    /**
     * Constructor: MspGis.Format.MSP
     * Create a new parser for MSP xmls.
     *
     * Parameters:
     * options - {Object} An object whose properties will be set on
     *     this instance. The measurePoints and measureField options are
     *     mandatory.
     */
    initialize: function(options) {
        if(!options || !options.measurePoints || !options.measureField) {
            OpenLayers.Console.error(
                "measurePoints or measureField options missing");
            return;
        }
        
        OpenLayers.Format.XML.prototype.initialize.apply(this, [options]);
        
        this.measureHash = {};
        var measurePoint, key;
        for(var i=0, len=this.measurePoints.length; i<len; ++i) {
            measurePoint = this.measurePoints[i];
            key = this.measurePathIdField ?
                measurePoint.attributes[this.measurePathIdField] :
                this.NO_PATHID_KEY;
            if(!this.measureHash[key]) {
                this.measureHash[key] = [];
            }
            this.measureHash[key].push(measurePoint);
        }
        
        var measureField = this.measureField;
        var sortMeasures = function(a, b) {
            var order;
            if (a.attributes[measureField] == b.attributes[measureField]) {
                order = 0;
            }
            else 
                if (a.attributes[measureField] > b.attributes[measureField]) {
                    order = 1;
                }
                else {
                    order = -1;
                }
            return order;
        }
        
        var measurePoints;
        for(var i in this.measureHash) {
            this.measureHash[i].sort(sortMeasures);
        }
    },
    
    /**
     * APIMethod: destroy
     */
    destroy: function() {
        OpenLayers.Format.XML.prototype.destroy.apply(this, arguments);
        this.measureHash = null;
        this.measurePoints = null;
    },
    
    /**
     * APIMethod: read
     * Read data from a string, and return a list of features. 
     * 
     * Parameters:
     * data - {String} or {DOMElement} data to read/parse.
     *
     * Returns:
     * {Array(<OpenLayers.Feature.Vector>)} An array of features.
     */
    read: function(data) {
        if(typeof data == "string") { 
            data = OpenLayers.Format.XML.prototype.read.apply(this, [data]);
        }

        var itemNodes = this.getElementsByTagNameNS(data,
            "#RowsetSchema", "row");

        var features = [];
        var attributes;
        for(var i=0, len=itemNodes.length; i<len; i++) {
            attributes = this.readAttributes(itemNodes[i]);
            attributes["kmText"] && features.push(new OpenLayers.Feature.Vector(
                this.getGeometry(
                    this.getMeasure(attributes),
                    this.getMeasurePathId(attributes)),
                attributes));
        }
        return features;
    },
    
    /**
     * Method: readAttributes
     * reads the attributes from the current node.
     * 
     * Parameters:
     * node - {DOMElement} node to extract attributes from
     * 
     * Returns:
     * {Object} a hash of the feature attributes
     */
    readAttributes: function(node) {
        var attributes = {};
        var attribute, name, reader;
        for(var i=0, len=node.attributes.length; i<len; ++i) {
            attribute = node.attributes[i];
            name = attribute.name;
            parser = this.attributeParsers[name];
            if(parser) {
                parser(attribute.value, attributes);
            } else {
                attributes[name] = attribute.value;
            }
        }
        return attributes;
    },
    
    /**
     * APIMethod: getMeasure
     * gets the measure value from attribute. This should be overridden at
     * instance creation to tell the reader how to get the measure, e.g.
     * 
     * (code)
     * getMeasure: function(attributes) {
     *     var mTokens = attribute.substr(
     *         attributes["kmText"].indexOf(" km ") + 3).split(/[- ]+km /);
     *     // get the avg measure location
     *     return 0.5 * (parseFloat(mTokens[0]) + parseFloat(mTokens[1]));
     * }
     * (end)
     * 
     * Parameters:
     * attributes - {Object} hash with the feature attributes; the measure
     *     has to be derived from one or more of these attributes
     * 
     * Returns:
     * {Float} the measure for the feature.
     */
    getMeasure: function(attributes) {
        var attribute = attributes["kmText"];
        var mTokens = attribute.substr(
            attribute.indexOf(" km ") + 3).split(/[- ]+km /);
        // get the avg measure location
        return 0.5 * (parseFloat(mTokens[0]) + parseFloat(mTokens[1]));
    },
    
    /**
     * APIMethod: getMeasurePathId
     * 
     * gets the path id for measuring the feature. This should be overridden at
     * instance creation to tell the reader how to get the path id, e.g.
     * 
     * (code)
     * getMeasurePathId: function(attributes) {
     *     return attributes["ows_Bahnachse"] || "Gleis 1";
     * },
     * (end)
     * 
     * Parameters:
     * attributes - {Object} hash with the feature attributes; the path id
     *     has to be derived from one or more of these attributes
     * 
     * Returns:
     * {Float} the path id for the feature.
     */
    getMeasurePathId: function(attributes) {
        return attributes["ows_Bahnachse"] || "Gleis_1";
    },
    
    /**
     * Method: getGeometry
     * Uses the measurePoints and measureField properties of this instance
     * to calculate interpolated coordinates for the passed measure.
     * 
     * Parameters:
     * measure - {Float} measure to get the coordinates for
     * 
     * Returns:
     * {<OpenLayers.Geometry.Point>} or null if the measure is outside the
     *     measurable range.
     */
    getGeometry: function(measure, pathId, left, right) {
        var measures = this.measureHash[pathId];
        left = left || 0;
        right = right || measures.length-1;
        var leftMeasure = measures[left].attributes[this.measureField];
        var rightMeasure = measures[right].attributes[this.measureField];
        
        // if we have the unlikely case of an exact match, we can stop here
        if(measure == leftMeasure) {
            return measures[left].geometry.clone();
        }
        if(measure == rightMeasure) {
            return measures[right].geometry.clone();
        }
        
        if (right - left > 1) {
            // we're not there yet
            var mid = Math.floor((left + right) / 2);
            var midMeasure = measures[mid].attributes[this.measureField];
            var isLeft = measure < midMeasure;
            return this.getGeometry(measure, pathId,
                isLeft ? left : mid, isLeft ? mid : right);
        } else {
            // now we're there
            var leftPoint = measures[left].geometry;
            var rightPoint = measures[right].geometry;
            var xDist = rightPoint.x - leftPoint.x;
            var yDist = rightPoint.y - leftPoint.y;
            var pos = (measure - leftMeasure) / (rightMeasure - leftMeasure);
            return new OpenLayers.Geometry.Point(
                leftPoint.x + (xDist * pos), leftPoint.y + (yDist * pos)); 
        }
    },
    
    CLASS_NAME: "OpenLayers.Format.MSP"
});
