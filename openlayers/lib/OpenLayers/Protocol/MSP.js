/**
 * @requires OpenLayers/Protocol.js
 */


OpenLayers.Protocol.MSP = OpenLayers.Class(OpenLayers.Protocol, {
    
    initialize: function(options) {
        OpenLayers.Protocol.HTTP.prototype.initialize.apply(this, arguments);
    },
    
    destroy: function() {
        OpenLayers.Protocol.HTTP.prototype.destroy.apply(this, arguments);
    },
    
    /**
     * Method: readResponse
     * Read HTTP response body and return features
     *
     * Parameters:
     * request - {Object} The request object
     *
     * Returns:
     * {Array({<OpenLayers.Feature.Vector>})} or
     * {<OpenLayers.Feature.Vector>} Array of features or a single feature.
     */
    readResponse: function(request) {
        var doc = request.responseXML;
        if (!doc || !doc.documentElement) {
            doc = request.responseText;
        }
        if (!doc || doc.length <= 0) {
            return null;
        }
        return this.format.read(doc);
    },

    read: function(options) {
        var mergedOptions = OpenLayers.Util.extend({}, options);
        OpenLayers.Util.applyDefaults(mergedOptions, this.options || {});

        var callback = function(request) {
            var resp = new OpenLayers.Protocol.Response({
                'features': null
            });
            if(request.status >= 200 && request.status < 300) {
                // success
                resp.features = this.readResponse(request);
                resp.code = OpenLayers.Protocol.Response.SUCCESS;
            } else {
                // failure
                resp.code = OpenLayers.Protocol.Response.FAILURE;
            }
            mergedOptions.callback.call(
                mergedOptions.scope, resp
            );
        }; 

        var data =
            '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">' +
            '  <soap12:Body>' +
            '    <GetListItems xmlns="http://schemas.microsoft.com/sharepoint/soap/">' +
            (this.params.listName ?
            '      <listName>' + this.params.listName + '</listName>' : '') +
            (this.params.viewName ?
            '      <viewName>' + this.params.viewName + '</viewName>' : '') +
            (this.params.rowLimit ?
            '      <rowLimit>' + this.params.rowLimit + '</rowLimit>' : '') +
            '    </GetListItems>' +
            '  </soap12:Body>' +
            '</soap12:Envelope>';
        
        return OpenLayers.Request.POST({
            url: this.url,
            callback: callback,
            data: data,
            headers: mergedOptions.headers,
            scope: this
        });       
    },
    
    CLASS_NAME: "OpenLayers.Protocol.MSP"
});
