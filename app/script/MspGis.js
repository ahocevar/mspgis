/**
 * Copyright (C) 2010-2012  Andreas Hocevar
 */

/**
 * @require GeoExt/widgets/MapPanel.js
 * @require GeoExt/widgets/form/GeocoderComboBox.js
 * @require GeoExt/data/ProtocolProxy.js
 * @require GeoExt/data/FeatureRecord.js
 * @require GeoExt/data/FeatureReader.js
 * @require GeoExt/widgets/Action.js
 * @require GeoExt/widgets/tree/WMSCapabilitiesLoader.js
 * @require GeoExt/widgets/tree/BaseLayerContainer.js
 * @require GeoExt/widgets/LegendPanel.js
 * @require GeoExt/widgets/WMSLegend.js
 * @require OpenLayers/Protocol/HTTP.js
 * @require OpenLayers/Format/GML/v3.js
 * @require OpenLayers/Kinetic.js
 * @require OpenLayers/Request/XMLHttpRequest.js
 * @require OpenLayers/Layer/Bing.js
 * @require OpenLayers/Layer/OSM.js
 * @require OpenLayers/Layer/SphericalMercator.js
 * @require OpenLayers/Layer/Vector.js
 * @require OpenLayers/Renderer/VML.js
 * @require OpenLayers/Renderer/SVG.js
 * @require OpenLayers/Control/ZoomPanel.js
 * @require OpenLayers/Control/Measure.js
 * @require OpenLayers/Control/Navigation.js
 * @require OpenLayers/Control/Attribution.js
 * @require OpenLayers/Control/PanPanel.js
 * @require OpenLayers/Control/ScaleLine.js
 * @require OpenLayers/Control/MousePosition.js
 * @require OpenLayers/Control/Permalink.js
 * @require OpenLayers/Control/WMSGetFeatureInfo.js
 * @require OpenLayers/Format/WKT.js
 * @require OpenLayers/Format/WMSGetFeatureInfo.js
 * @require OpenLayers/Format/WMSCapabilities/v1_3_0.js
 * @require OpenLayers/Popup/FramedCloud.js
 * @require OpenLayers/Rule.js
 * @require OpenLayers/Handler/Path.js
 * @require OpenLayers/Handler/Polygon.js
 * @require Base64.js
 * @require proj4js-combined.js
 */

Proj4js.defs['M15'] = '+proj=tmerc +lat_0=0 +lon_0=15 +k=1.000000 +x_0=0 +y_0=-5000000 +ellps=bessel +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs';
Proj4js.defs['EPSG:3857'] = Proj4js.defs['EPSG:900913'];
(function() {

var bingKey = "ArnKR3Rv3xqzMWLrxwun19Chw5J4lrEEWizKYuGEs1641VGFS_csdpxtzoweoUC4";
var bingCulture = navigator.language || "de";

window.MspGis = {
    
    owsWrapper: "/GisServices/bild.asp?name=",
    wrapUrls: [
        "http://as01060.bau.oebb.at/cgi-bin/mapserv?",
        "http://as00971.bau.oebb.at/", //ArcGIS
        "http://as02982.bau.oebb.at/", //FUGRO
        "http://as01057.bau.oebb.at/ImageX/ecw_wms.dll?"
    ],
    baseUrl: "http://as00971.bau.oebb.at/ArcGISVD/rest/services/",
    hiddenFields: ["Id", "Shape"],
    mspHost: "http://msp.bau.oebb.at",
    extentLayer: "Baulose",
    extentNameAttr: "Baulosname",

    layers: [
        new OpenLayers.Layer("Keine Grundkarte", {isBaseLayer: true, numZoomLevels: 19}),
        new OpenLayers.Layer.OSM(),
        new OpenLayers.Layer.Bing({
            key: bingKey,
            type: "Road",
            name: "Bing Straßenkarte",
            culture: bingCulture
        }),
        new OpenLayers.Layer.Bing({
            key: bingKey,
            type: "AerialWithLabels",
            name: "Bing Orthophoto beschriftet",
            culture: bingCulture
        }),
        new OpenLayers.Layer.Bing({
            key: bingKey,
            type: "Aerial",
            name: "Bing Orthophoto",
            culture: bingCulture
        }),
        new OpenLayers.Layer.WMS("Orthophoto IWS", "http://as01057.bau.oebb.at/ImageX/ecw_wms.dll?orthopka?", {
            layers: "PKA_SPM",
            format: "image/jpeg"
        }, {
            singleTile: true,
            ratio: 1,
            maxResolution: 152.874056542968766,
            numZoomLevels: 13
        }),
        new OpenLayers.Layer.WMS("Infrarotbild IWS", "http://as01057.bau.oebb.at/ImageX/ecw_wms.dll?orthopkainfra?", {
            layers: "PKAIR_SPM",
            format: "image/jpeg"
        }, {
            singleTile: true,
            ratio: 1,
            maxResolution: 152.874056542968766,
            numZoomLevels: 13
        }),
        new OpenLayers.Layer.WMS("ÖK 50 IWS", "http://as01057.bau.oebb.at/ImageX/ecw_wms.dll?orthooek?", {
            layers: "OEK_SPM",
            format: "image/jpeg"
        }, {
            singleTile: true,
            opacity: 0.5,
            ratio: 1,
            maxResolution: 152.874056542968766,
            numZoomLevels: 9
        }),
        new OpenLayers.Layer.WMS("Inhalte", null,
            {format: "image/png", transparent: true},
            {isBaseLayer: false, singleTile: true, ratio: 1, visibility: false, projection: "EPSG:3857"}
        )
    ],
    
    extents: null,
    namedExtentsBounds: null,
    refBounds: null,
    extentsCombo: null,
    map: null,
    popup: null,
    tools: null,
    mergeParamsTimer: null,
    wmsLayers: null,
    params: null,
    
    initUI: function(callback) {
        Ext.onReady(function() {
            Ext.QuickTips.init();
            this.createTools();
            this.createMap(callback);
        }, this);
    },
    
    activate: function() {
        var mapPanel = GeoExt.MapPanel.guess();
        mapPanel.extent = this.refBounds || this.namedExtentsBounds;
        this.map.addLayers(this.layers);
        this.createControls();
    },
    
    createExtents: function() {
        this.extents = new Ext.data.Store({
            reader: new GeoExt.data.FeatureReader({}, [
                {name: 'name', mapping: this.extentNameAttr},
                {name: 'bounds', convert: function(v, feature) {
                    var bounds = feature.geometry.getBounds();
                    if (!this.namedExtentsBounds) {
                        this.namedExtentsBounds = bounds.clone();
                    } else {
                        this.namedExtentsBounds.extend(bounds);
                    }
                    return bounds.toArray();
                }}
            ]),
            proxy: new GeoExt.data.ProtocolProxy({
                protocol: new OpenLayers.Protocol.HTTP({
                    url: this.encodeUrl(this.url.replace("/rest/", "/") + "/MapServer/WFSServer?" +
                        OpenLayers.Util.getParameterString({
                            SERVICE: "WFS",
                            VERSION: "1.1.0",
                            REQUEST: "GetFeature",
                            TYPENAME: this.extentLayer,
                            BBOX: this.projectExtent ? this.projectExtent.clone().transform('EPSG:3857', 'EPSG:4326').toBBOX() : undefined
                        })),
                    format: new OpenLayers.Format.GML.v3()
                })
            })
        });
    },

    loadExtents: function(callback) {
        this.extents.load({
            callback: callback
        });
    },
    
    loadRef: function(callback) {
        var params = this.params;
        var url;
        if (params.ref) {
            var ref = params.ref.replace(",", ".").split(";");
            if (ref.length === 3) {
                url = this.encodeUrl('http://as00971.bau.oebb.at/webgis/get_kmbbox_json.php?' +
                    Ext.urlEncode({kmvon: ref[0], kmbis: ref[1], Gleis: ref[2]})
                );
            } else if (ref.length === 2) {
                url = this.encodeUrl('http://as00971.bau.oebb.at/webgis/getrest_mspid_json.php?' +
                    Ext.urlEncode({MSPID: ref[0], LAYER: ref[1]})
                );
                if (!~OpenLayers.Util.indexOf(this.wmsLayers, ref[1])) {
                    this.wmsLayers.push(ref[1]);
                    this.updateVisibility({});
                }
            }
            Ext.Ajax.request({
                url: url,
                success: function(response) {
                    var result = Ext.decode(response.responseText);
                    var extent = OpenLayers.Geometry.fromWKT(result.results[0].FBBOX).getBounds();
                    if (!Ext.isNumber(extent.left) || !Ext.isNumber(extent.top) || !Ext.isNumber(extent.right) || !Ext.isNumber(extent.bottom)) {
                        extent = this.map.getMaxExtent();
                    }
                    this.refBounds = extent;
                    callback();
                },
                failure: callback,
                scope: this
            });
        } else {
            callback();
        }
    },
    
    createMap: function(callback) {
        var mercator = new OpenLayers.Projection("EPSG:900913");
        var wgs84 = new OpenLayers.Projection("EPSG:4326");
        this.map = new OpenLayers.Map({
            displayProjection: wgs84,
            projection: mercator,
            units: "m",
            maxResolution: 156543.0339,
            maxExtent: new OpenLayers.Bounds(-20037508.34, -20037508.34,
                                             20037508.34, 20037508.34),
            controls: [],
            eventListeners: {
                "buttonclick": function(evt) {
                    if (OpenLayers.Element.hasClass(evt.buttonElement, "closelayerinfo")) {
                        evt.buttonElement.parentNode.style.display = "none";
                    }
                },
                "zoomend": this.updateVisibility,
                scope: this
            }
        });
        var app = this;
        new Ext.Viewport({
            layout: "border",
            items: [{
                id: "mappanel",
                xtype: "gx_mappanel",
                region: "center",
                map: this.map,
                tbar: [new GeoExt.form.GeocoderComboBox({
                    tooltip: "Zoom nach Suchbegriff. Aufziehen eines Zoom-Rechtecks in der Karte mit gedrückter Shift-Taste ist ebenfalls möglich.",
                    srs: "EPSG:900913",
                    emptyText: 'Kartenausschnitt wählen',
                    mode: "local",
                    triggerAction: "all",
                    store: this.extents,
                    listeners: {
                        render: function() {
                            Ext.QuickTips.register({
                                target: this.el,
                                text: this.tooltip
                            });
                        },
                        beforequery: function(qe) {
                            var len = qe.query.length;
                            qe.query = new RegExp(qe.query, "i");
                            qe.query.length = len;
                        }
                    }
                }), new GeoExt.form.GeocoderComboBox({
                    tooltip: 'Suchbegriff eingeben',
                    srs: 'EPSG:900913',
                    emptyText: 'Suchbegriff eingeben',
                    valueField: 'Shape',
                    tpl: '<tpl for="."><div class="x-combo-list-item">[{v_table}] {s_value}</div></tpl>',
                    queryParam: 'TXT',
                    store: new Ext.data.JsonStore({
                        root: 'results',
                        fields: ['s_value', 'v_table', {
                            name: 'Shape',
                            convert: function(v, rec) {
                                var geom = OpenLayers.Geometry.fromWKT(rec.Shape);
                                return geom.getBounds().toArray();
                            }
                        }],
                        proxy: new Ext.data.HttpProxy({
                            url: 'http://as00971.bau.oebb.at/webgis/get_ftxtquery_json.php',
                            listeners: {
                                beforeload: function(proxy, params) {
                                    params.LAYER = this.wmsLayers.join(',');
                                    params._dc = new Date().getTime();
                                    proxy.setUrl(this.encodeUrl(
                                        Ext.urlAppend(proxy.url, Ext.urlEncode(params))
                                    ));
                                    for (var p in params) {
                                        delete params[p];
                                    }
                                    proxy.conn.disableCaching = false;
                                },
                                scope: this
                            }
                        }),
                        listeners: {
                            load: function(store, records) {
                                if (this.projectExtent) {
                                    for (var i=records.length-1; i>=0; --i) {
                                        var bounds = OpenLayers.Bounds.fromArray(records[i].get('Shape'));
                                        if (!bounds.intersectsBounds(this.projectExtent)) {
                                            store.remove(records[i]);
                                        }
                                    }
                                }
                            },
                            scope: this
                        }
                    })
                }), "-", new GeoExt.Action({
                    control: new OpenLayers.Control(),
                    map: this.map,
                    tooltip: "Navigation",
                    iconCls: "navigation",
                    pressed: true,
                    toggleGroup: "tools"
                }), new GeoExt.Action({
                    control: this.tools.measureArea,
                    map: this.map,
                    iconCls: "measure-area",
                    tooltip: "Fläche messen",
                    toggleGroup: "tools"
                }), new GeoExt.Action({
                    control: this.tools.measureDistance,
                    map: this.map,
                    iconCls: "measure-distance",
                    tooltip: "Distanz messen",
                    toggleGroup: "tools"
                })]
            }, {
                xtype: "tabpanel",
                region: "east",
                width: 200,
                collapsible: true,
                collapseMode: "mini",
                hideCollapseTool: true,
                split: true,
                activeTab: 0,
                items: [{
                    id: "treepanel",
                    xtype: "treepanel",
                    title: "Layer",
                    width: 200,
                    autoScroll: true,
                    rootVisible: false,
                    lines: false,
                    useArrows: true,
                    root: new Ext.tree.AsyncTreeNode({
                        children: [{
                            text: "Kartenthemen",
                            expanded: true,
                            loader: new MspGis.ArcGISRestLoader({
                                url: this.encodeUrl(this.url + "/MapServer/layers?f=json"),
                                // customize the createNode method to add a checkbox to nodes
                                createNode: function(attr) {
                                    if (attr.layer) {
                                        attr.checked = false;
                                        attr.expanded = false;
                                        if (attr.layer.metadata.description) {
                                            attr.qtip = attr.layer.metadata.description;
                                        }
                                    }
                                    return MspGis.ArcGISRestLoader.prototype.createNode.apply(this, [attr]);
                                },
                                listeners: {
                                    load: function(loader, node) {
                                        this.updateVisibility({firstRun: true});
                                        this.updateNodes();
                                        node.getOwnerTree().getRootNode().appendChild(new GeoExt.tree.BaseLayerContainer({
                                            text: "Grundkarte",
                                            expanded: true
                                        }));
                                        callback();
                                    },
                                    scope: this
                                }
                            })
                        }]
                    }),
                    listeners: {
                        checkchange: this.checkChange,
                        scope: this
                    }
                }, {
                    xtype: "gx_legendpanel",
                    title: "Legende",
                    padding: 5,
                    autoScroll: true,
                    defaults: {
                        cls: "legend",
                        showTitle: false,
                        // dragons ahead - non-API override
                        getLegendUrl: function() {
                            var url = Ext.ComponentMgr.types[this.getXType()].prototype.getLegendUrl.apply(this, arguments);
                            return app.encodeUrl(url);
                        }
                    }
                }]}
            ]
        });
        new Ext.LoadMask(GeoExt.MapPanel.guess().body, {
            msg: "Kartenausschnitte werden geladen...",
            store: this.extents
        }).show();
    },
    
    checkchanging: false,
    checkChange: function(node, checked) { 
        if (!this.checkchanging) {
            var i, nodes = node.childNodes;
            for (i=nodes.length-1; i>=0; --i) {
                nodes[i].ui.toggleCheck(checked);
            }
            node = node.parentNode;
            if (node) {
                this.checkchanging = true;
                var children = node.childNodes, numChecked = 0;
                for (i=children.length-1; i>=0; --i) {
                    numChecked += children[i].ui.isChecked() + 0;
                }
                var allChildrenChecked = numChecked === children.length;
                node.ui.toggleCheck(allChildrenChecked);
                if (this.updatingVisibility) {
                    var collapse = allChildrenChecked || numChecked === 0;
                    node[collapse ? "collapse" : "expand"](undefined, false);
                }
                this.checkchanging = false;
            }
            var root = node.getOwnerTree().getRootNode();
            var maxExtent, layers = [];
            root.firstChild.cascade(function(n) {
                if (n.attributes.loader instanceof MspGis.ArcGISRestLoader) {
                    var layer = n.attributes.layer,
                        capability = layer && layer.metadata,
                        name = capability && capability.name;
                    if (n.ui.isChecked() && name) {
                        var extent = layer.maxExtent;
                        if (!maxExtent) {
                            maxExtent = extent;
                        } else {
                            maxExtent.extend(extent);
                        }
                        if (n.isLeaf()) {
                            layers.push(capability.name);
                        }
                    }
                }
            });
            this.wmsLayers = layers;
            if (this.mergeParamsTimer) {
                window.clearTimeout(this.mergeParamsTimer);
            }
            var layer = this.layers[this.layers.length - 1],
                url = this.url;
            this.mergeParamsTimer = window.setTimeout(function() {
                layer.addOptions({
                    maxExtent: maxExtent,
                    url: url.replace("/rest/", "/") + "/MapServer/WMSServer"
                });
                layer.mergeNewParams({layers: layers});
                layer.setVisibility(!!layers.length);
            }, 200);
        }
    },
    
    updatingVisibility: false,
    updateVisibility: function(evt) {
        var tree = Ext.getCmp("treepanel"),
            target = tree.getRootNode().firstChild;
            wmsLayers = [].concat(this.wmsLayers);
        if (target) {
            this.updatingVisibility = true;
            target.cascade(function(n) {
                if (!n.attributes.layer) {
                    return;
                }
                // WMSCapabilitiesLoader cannot do preloadChildren, so we expand
                // temporarily to have children loaded
                var expanded = n.isExpanded();
                n.expand(undefined, false);
                if (!expanded) {
                    n.collapse(undefined, false);
                }
                var capability = n.attributes.layer.metadata,
                    scale = this.map.getScale();
                if (scale <= (capability.minScale || Number.POSITIVE_INFINITY) && scale >= (capability.maxScale || 0)) {
                    n.enable();
                } else {
                    n.disable();
                }
                if (evt.firstRun) {
                    var name = capability.name,
                        checked = !!(~wmsLayers.indexOf(name));
                    if (name && n.ui.isChecked() !== checked) {
                        n.ui.toggleCheck(checked);
                    }
                }
            }, this);
            this.updatingVisibility = false;
        }
    },
    
    updateNodes: function() {
        var tree = Ext.getCmp("treepanel"),
            target = tree.getRootNode().firstChild,
            projectExtent = this.projectExtent;
        if (projectExtent) {
            target.cascade(function(n) {
                var layer = n.attributes.layer;
                if (layer) {
                    n.ui[layer.maxExtent.intersectsBounds(projectExtent) ? 'show' : 'hide']();
                }
            });
        }
    },
    
    createControls: function() {
        var app = this;
        var gfi = new OpenLayers.Control.WMSGetFeatureInfo({
            autoActivate: true,
            layers: [this.layers[this.layers.length-1]],
            queryVisible: true,
            infoFormat: "text/html",
            // no parser needed for html
            format: {read: function(doc) {return doc; }},
            eventListeners: {
                "getfeatureinfo": this.getFeatureInfo,
                scope: this
            },
            buildWMSOptions: function(url, layers, clickPosition, format) {
                var options = OpenLayers.Control.WMSGetFeatureInfo.prototype.buildWMSOptions.apply(this, arguments);
                var params = OpenLayers.Util.getParameterString(options.params);
                OpenLayers.Util.extend(params, OpenLayers.Util.getParameters(options.url));
                var newUrl = OpenLayers.Util.urlAppend(options.url.split("?")[0], params);
                options.url = app.encodeUrl(newUrl);
                delete options.params;
                options.failure = function(){};
                options.success = options.callback;
                delete options.callback;
                return options;
            }
        });
        var that = this;
        var permalink = new OpenLayers.Control.Permalink(null, window.location.href.replace(/[?&]ref=[^&]*/g, ""), {
            title: "Öffnet Karte in eigenem Fenster. Link speichern für Lesezeichen.",
            createParams: function(center, zoom, layers) {
                var params = OpenLayers.Control.Permalink.prototype.createParams.apply(this, arguments);
                if (that.wmsLayers.length) {
                    params.l = that.wmsLayers;
                }
                // turn all overlays off - we control overlay visibility ourselves
                params.layers = params.layers.replace("T", "F");
                return params;
            }
        });
        var controls = [
            new OpenLayers.Control.Attribution(),
            new OpenLayers.Control.Navigation({
                mouseWheelOptions: {interval: 50},
                dragPanOptions: {enableKinetic: true}
            }),
            new OpenLayers.Control.PanPanel({
                title: "Kartenausschnitt verschieben"
            }),
            new OpenLayers.Control.ZoomPanel({
                title: "Zoom"
            }),
            new OpenLayers.Control.ScaleLine({geodesic: true}),
            new OpenLayers.Control.MousePosition({
                formatOutput: function(lonLat) {
                    return OpenLayers.Util.getFormattedLonLat(lonLat.lat) +
                        ", " +
                        OpenLayers.Util.getFormattedLonLat(lonLat.lon, "lon");
                }
            }),
            new OpenLayers.Control.MousePosition({
                displayClass: 'mouseposition-m15',
                displayProjection: 'M15',
                formatOutput: function(xy) {
                    return 'M15: ' + Math.round(xy.lon) + ", " + Math.round(xy.lat);
                }
            }),
            permalink,
            gfi
        ];
        this.map.addControls(controls);
        permalink.element.target = "_blank";
        permalink.element.innerHTML = "Vollbild / Permalink";        
    },
	
	createTools: function() {
        var sketchSymbolizers = {
            "Point": {
                pointRadius: 4,
                graphicName: "square",
                fillColor: "white",
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: "#333333",
                label: "${getLabel}",
                labelAlign: "rm",
                labelXOffset: -10,
                fontSize: "11pt" 
            },
            "Line": {
                strokeWidth: 3,
                strokeOpacity: 1,
                strokeColor: "#666666",
                strokeDashstyle: "dash"
            },
            "Polygon": {
                strokeWidth: 2,
                strokeOpacity: 1,
                strokeColor: "#666666",
                fillColor: "white",
                fillOpacity: 0.3
            }
        };
        var context = {
            "getLabel": function(feature) {
                return (feature.attributes && feature.attributes.label) ?
                    feature.attributes.label : "";
            }
        };
        var style = new OpenLayers.Style(null, {
            context: context
        });
        style.addRules([new OpenLayers.Rule({
            symbolizer: sketchSymbolizers
        })]);
        var styleMap = new OpenLayers.StyleMap({"default": style});
        
        var Measure = OpenLayers.Class(OpenLayers.Control.Measure, {
            initialize: function(handler, options) {
                this.eventListeners.scope = this;
                OpenLayers.Control.Measure.prototype.initialize.apply(this, arguments);
            },
            unitsAppend: "",
            geodesic: true,
			persist: true,
            handlerOptions: {
                layerOptions: {styleMap: styleMap}
            },
            callbacks: {
                "modify": function(geom, sketch){
                    OpenLayers.Control.Measure.prototype.measurePartial.call(
                        this, geom, sketch.geometry);
                }
            },
            eventListeners: {
                "activate": function() {
                    delete this.handler.callbacks["create"];
                },
                "measurepartial": function(evt) {
                    var handler = evt.object.handler;
                    handler.point.attributes.label =
                        OpenLayers.Number.format(evt.measure, 2, ".", ",") +
                        " " + evt.units + this.unitsAppend;
				},
                "measure": function(evt) {
                    var handler = evt.object.handler;
                    handler.point.attributes.label =
                        OpenLayers.Number.format(evt.measure, 2, ".", ",") +
                        " " + evt.units + this.unitsAppend;
                    handler.layer.addFeatures([handler.point], {silent: true});
                }
            }
        });
        
        var measureDistance = new Measure(OpenLayers.Handler.Path);

        var measureArea = new Measure(OpenLayers.Handler.Polygon,
            {unitsAppend: "²"});

		this.tools = {
			"measureDistance": measureDistance,
			"measureArea": measureArea
        };
    },
        
    getFeatureInfo: function(evt) {
        var loc = window.location;
        var match = evt.text.match(/<body[^>]*>([\s\S]*)<\/body>/);
        if (match && !match[1].match(/^\s*$/)) {
            var info = match[1];
            info = info.replace(new RegExp(this.mspHost, "g"), loc.protocol + "//" + loc.host);
            info = info.replace(/ href=/g, ' target="_blank" href=');
            if(this.popup) {
                this.map.removePopup(this.popup);
                this.popup.destroy();
            }
            this.popup = new OpenLayers.Popup.FramedCloud("Popup", 
                this.map.getLonLatFromPixel(evt.xy),
                new OpenLayers.Size(100, 100),
                info,
                null, true);
            this.map.addPopup(this.popup);
        }
    },

    encodeUrl: function(url) {
        for (var i=this.wrapUrls.length-1; i>=0; --i) {
            var local = this.wrapUrls[i].replace(".bau.oebb.at/", "/");
            if (url.indexOf(local === 0)) {
                url = url.replace(local, this.wrapUrls[i]);
            }
            if (url.indexOf(this.wrapUrls[i]) === 0) {
                url = this.owsWrapper + Base64.encode(url);
                break;
            }
        }
        return url;
    },
    
    dispatch: function(functions, complete, scope) {
        complete = complete || Ext.emptyFn;
        scope = scope || this;
        var requests = functions.length;
        var responses = 0;
        var storage = {};
        function respond() {
            ++responses;
            if(responses === requests) {
                complete.call(scope, storage);
            }
        }
        function trigger(index) {
            window.setTimeout(function() {
                functions[index].apply(scope, [respond, storage]);
            });
        }
        for(var i=0; i<requests; ++i) {
            trigger(i);
        }
    }
    
};

(function() {
    
    Ext.BLANK_IMAGE_URL = 'externals/ext/resources/images/default/s.gif';
    OpenLayers.ImgPath = 'externals/openlayers/img/';
    OpenLayers.IMAGE_RELOAD_ATTEMPTS = 10;
    var app = this;
    var origGetURL = OpenLayers.Layer.WMS.prototype.getURL;
    OpenLayers.Layer.WMS.prototype.getURL = function(){
        return app.encodeUrl(origGetURL.apply(this, arguments));
    };
    var params = Ext.urlDecode(window.location.href.split('?').pop());
    this.wmsLayers = params.l.split(",") || [];
    if (params.map) {
        this.url = this.baseUrl + params.map;
    } else if (params.rest) {
        this.url = params.rest;
    } else {
        this.url = this.baseUrl + 'PKA_MSP';
    }
    if (params.extent) {
        this.projectExtent = OpenLayers.Bounds.fromString(params.extent);
    }
    this.params = params;
    
    this.createExtents();
    this.dispatch([
        this.loadExtents,
        this.initUI,
        this.loadRef
    ], this.activate, this);
    
}).call(MspGis);
}());
