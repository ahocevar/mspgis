/*
 * @require MspGis.js
 */
MspGis.ArcGISRestLoader = function(config) {
    Ext.apply(this, config);
    MspGis.ArcGISRestLoader.superclass.constructor.apply(this, arguments);
};

Ext.extend(MspGis.ArcGISRestLoader, Ext.tree.TreeLoader, {
    
    url: null,
    
    added: null,
    
    processResponse : function(response, node, callback, scope){
        if (response.status < 200 || response.status > 299) {
            if (!this._retry) {
                this._retry = true;
            } else {
                var r = window.confirm("Server antwortet nicht. Erneut versuchen?");
                if (r === true) {
                    delete this._retry;
                } else {
                    return;
                }
            }
            this.load(node, callback, scope);
            return;
        }
        delete this._retry;
        var capability = Ext.decode(response.responseText);
        if (capability.layers) {
            this.added = [];
            this.processLayers(capability.layers, capability.layers, node);
        }
        if (typeof callback == "function") {
            callback.apply(scope || node, [node]);
        }
    },
    
    processLayers: function(localLayers, layers, node) {
        for (var i=0, ii=localLayers.length; i<ii; ++i) {
            var el = localLayers[i];
            if (~this.added.indexOf(el)) {
                continue;
            }
            if (!el.type) {
                // replace subLayer object with the real stuff
                for (var j=layers.length-1; j>=0; --j) {
                    if (layers[j].id === el.id) {
                        el = layers[j];
                        this.added.push(el);
                        break;
                    }
                }
            }
            var n = this.createNode({text: el.name, 
                // use nodeType 'node' so no AsyncTreeNodes are created
                nodeType: 'node',
                layer: this.createWMSLayer(el, this.url),
                leaf: (el.subLayers.length === 0)});
            if(n){
                node.appendChild(n);
            }
            if (el.subLayers) {
                this.processLayers(el.subLayers, layers, n);
            }
        }
    },
    
    createWMSLayer: function(layer, url) {
        if (layer.name) {
            var extent = layer.extent;
            return new OpenLayers.Layer.WMS( layer.name, url,
                OpenLayers.Util.extend({format: "image/png", 
                    layers: layer.name}, this.layerParams),
                OpenLayers.Util.extend({minScale: layer.minScale,
                    queryable: !!~layer.capabilities.indexOf('Query'),
                    maxScale: layer.maxScale,
                    maxExtent: new OpenLayers.Bounds(extent.xmin, extent.ymin, extent.xmax, extent.ymax),
                    metadata: layer
                }, this.layerOptions));
        } else {
            return null;
        }
    }
    
});